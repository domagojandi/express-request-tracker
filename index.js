function requestTracker(cb) {
	var callback = cb || function(){};

	return function logger(req, res, next) {
		var startTime = Date.now();

		res.on('finish', function() {
			callback({
				'timestamp': (startTime / 1000) | 0,
				'request': {
					'endpoint': req.path,
					'protocol': req.protocol,
					'duration': Date.now() - startTime,
					'headers': req.headers,
					'body': req.body,
					'queryParams': req.query,
					'urlParams': req.params
				},
				'response': {
					'code': this.statusCode,
					'headers': this._headers,
					'length': this._contentLength
				},
				'remoteAddress': req.connection && req.connection.remoteAddress
			});
		});
		next();
	};
}

module.exports = requestTracker;

# EXPRESS REQUEST TRACKER #
This express.js middleware can be used for tracking requests.

## HOW TO USE ##
Here is an example code snippet which explains how to use this module:
```javascript
const tracker = require('express-request-tracker');
const express = require('express');
const EventEmitter = require('events').EventEmitter;

const app = express();
const emitter = new EventEmitter();
app.use(tracker(data => {
    emitter.emit('request-tracker', data);
}));

// ... then, somewhere in the code ...
emitter.on('request-tracker', data => {
    fluentd.emit('request', data);
});
```